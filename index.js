const MongoClient = require('mongodb').MongoClient
const lolcounter = require('./lolcounter.js')
const riotgames = require('./riotgames.js')
const assert = require('assert')

const url = 'mongodb://localhost:27017'

MongoClient.connect(url, (error, client) => {
	assert.equal(null, error)
	console.log('connected')

	const lolcounterdb = client.db('lolcounter')
	const staticdatadb = client.db('staticdata')

	riotgames.initial(staticdatadb)
	.then(response => lolcounter.initial(lolcounterdb, response))
	.catch(error => console.log(error))
})





//http://lolcounter.com/champions/aatrox/weak/general
// lolcounter has identifiers
// each identifier has id, key, name, parameter, and relationships
// each relationship has name and lanes ['weak', 'strong', 'good'] ['general', 'top', 'mid', 'jungle', 'bottom']
// each lane has name and matchups 
// each matchup has text, lane, upvote, downvote, and tips
// each tip has vote and text

// initialize the database
// i