const http = require('http')
const cheerio = require('cheerio')

module.exports.updateOne = function(db, response){
	//todo
}
module.exports.initial = function(db, response){
	return new Promise((resolve,reject)=>{
		begin(db, response)
		.then(matchups)
		.then(resolve)
		.catch(error => reject(error))
	})
}

function begin(db, response){
	return new Promise((resolve,reject) => {
		let lanes = { general: new Array(),
					top: new Array(),
					mid: new Array(),
					jungle: new Array(),
					bottom: new Array()
				}
		let lolcounter = []
		let parameters = []
		let ids = Object.getOwnPropertyNames( response['keys'] )
		for(let id of ids){
			let name = response['data'][id]['name']
			let parameter = name.replace(/ [A-Z]/, (match, index, original)=>{
				let string = '-'
				string += original.charAt(index+1).toLowerCase()
				return string
			})
			parameter = parameter.replace(/'[A-Z]/, (match, index, original)=>{
				return original.charAt(index+1).toLowerCase()
			})			
			parameter = parameter.replace('.', (match, index, original)=>{
				return ''
			})
			parameter = parameter.toLowerCase()
			parameters.push(parameter)

			lolcounter.push({id:id, 
							parameter:parameter, 
							weak:lanes, 
							strong:lanes, 
							even:lanes, 
							good:lanes, 
							tips:new Array()})
		}
		db.collection('matchup').createIndex({id:1}, {unique:true}, (error, result) =>{
			console.log('lolcounter index created')
			db.collection('matchup').insert(lolcounter, (error, result)=>{
				if(error === null){
					console.log('lolcounter initialized')
					resolve(new Array(db, lolcounter, parameters, ids, response['keys']))			
				}
				else if(error.name === 'BulkWriteError'){
					console.log('lolcounter already exists')
					resolve(new Array(db, lolcounter, parameters, ids, response['keys']))
				}
				else reject(error)

			})
		})
	})
}

function matchups(a){ // a = new Array(db, lolcounter, parameters, ids, keys)
	return new Promise((resolve,reject)=>{
		let db = a[0]
		let lolcounter = a[1]
		let parameters = a[2]
		let ids = a[3]

		let relationships = ['weak', 'strong', 'even', 'good']
		let lanes = ['general', 'top', 'mid', 'jungle', 'bottom']


		for(let pi of parameters.keys()){
			let parameter = parameters[pi]
			let id = ids[pi]

			for(let relation of relationships){

				for(let lane of lanes) {				
					request(new Array(parameter, relation, lane, a[4]))
					.then(scrape)
					.then(array => {
						db.collection('matchup').findOne({id:id}, (error, result)=>{
							if(error) reject(error)
							else{
								result[relation][lane] = array
								db.collection('matchup').findOneAndUpdate({id:id}, result, (error, result)=>{
									console.log(`${parameter} ${relation} ${lane} updated`)
									resolve()
								})
							}
						})
					})
					.catch(error => reject(error))		
				}
			}
		}	
	})
}

function request(a){
	return new Promise((resolve,reject)=>{
		let response = ''
		let url = `http://lolcounter.com/champions/${a[0]}/${a[1]}/${a[2]}`
		const req = http.request(url, res =>{
			res.on('data', d => {
				response += d
			})
			res.on('end', () =>{
				resolve(new Array(response, a[3]))
			})
		})
		req.on('error', error => reject(error))
		req.end()
	})
}

function scrape(a){
	return new Promise((resolve,reject)=>{
		let $ = cheerio.load(a[0])
		
		let keys = a[1]
		let ids = []
		$('.block3 .name').each((i,e)=>{
			let name = e.children[0].data
			name = name.replace(' ', '')
			name = name.replace('.', '')
			name = name.replace('\'', '') 

			for(let key in keys) 
				if(keys[key] === name)
					ids.push(key)

			if(name === 'Wukong') 
				ids.push(62)
		})

		let upvotes = []
		$('.uvote').each( (i,e)=> upvotes.push(e.children[1].data) )

		let downvotes = []
		$('.dvote').each( (i,e)=> downvotes.push(e.children[1].data) )

		let deck = []
		ids.forEach((current, index)=>{
			let card = {}
			card['id'] = current
			card['upvote'] = upvotes[index]
			card['downvote'] = downvotes[index]
			deck.push(card)
		})
		resolve(deck)
	})
}