const https = require('https')
const fs = require('fs')

module.exports.initial = function(db){
	return new Promise((resolve,reject)=>{
		requestChampions(db)
		.then(write)
		.then(keys=>resolve(keys))
		.catch(error=>reject(error))
	})
}

function requestChampions(db){
	return new Promise((resolve,reject)=>{
		const header = {	
						    'Accept-Charset': 'application/x-www-form-urlencoded; charset=UTF-8',
						    'X-Riot-Token': 'RGAPI-ebd2137f-14d0-4141-947c-fcb431e564da',
						    'Accept-Language': 'en-US,en;q=0.9',
						}

		const options = {
							'hostname': 'na1.api.riotgames.com',
							'path': `/lol/static-data/v3/champions?locale=en_US&tags=keys&dataById=true`,
							'headers': header,
							'agent': false,
						}

		const request = https.request(options, response=>{
			let buffer = ''
			response.on('data', chunk=>buffer += chunk)
			response.on('end', ()=>resolve(new Array(db, JSON.parse(buffer))))

		})
		request.on('error', error=>reject(error))
		request.end()
	})
}

function write(a){
	return new Promise((resolve,reject)=>{
		let db = a[0]
		let response = a[1]

		db.collection('champion').createIndex({version:1}, {unique:true}, (error, result) =>{
			console.log('index created')
		})
		db.collection('champion').createIndex({status:1}, {unique:true}, (error, result) =>{
			console.log('index created')
		})
		db.collection('champion').insert(response, (error, result) =>{
			if(error === null){
				console.log('response written')
				resolve(response)
			}
			else if(error.name === 'BulkWriteError'){
				console.log('static data response already exists')
				db.collection('champion').findOne({version:Number}, (error, result) =>{
					if(error) module.initial(db)
					else resolve(result)
				})
			}
			else reject(error)
		})
	})
}